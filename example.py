import pathlib
import json
import sys
from input import *

#Query parameters we are providing dynamically 
qr_prms=input("Enter  query parameters for solutions endpoint :")

#Converting query parameters to dict
qry_prm=query_params(qr_prms)

#Loading all solution.json files to tuple
manifests=all_manifests()

#Getting members data for individual member
def get_members(offset,limit):
    keys_in_members = ["solutionId","contextProfile","description","registrations"]
    members_data=list(dict.fromkeys(keys_in_members))
    members_data = []
    for manifest in manifests[offset:limit]:
       empty_member_data = {}
       for key in keys_in_members:
          empty_member_data[key] = manifest[key]
       members_data.append(empty_member_data)
    print("members length",len(members_data))
    members=(json.dumps(members_data,indent=4))
    return members



#This function will take user input and provide response based on request
def get_solutions(solutions,qry_prm=None,):
    

    offset=0
    limit=0
    member_ids=[]
    solution_ids=[]
    installed_solutions=0
    include_member=False

    if qry_prm!=None:
       if 'offset' in qry_prm:
         offset=int(qry_prm['offset'])
    
       if 'limit' in qry_prm:
          limit=int(qry_prm['limit'])
       if 'include_member' in qry_prm: 
          include_member=bool('include_member')
          print(include_member)
        

    if len(solutions)>0:
      for solution in solutions:
         installed_solutions+=1

    
    if offset<0 or limit<=0:
        print("provide query parameters are not valid")
        sys.exit()
        
    elif offset>installed_solutions:
        solution_ids=[]
    else:
        last= limit if limit!=0 else installed_solutions
        for i in range(offset,last):
          if i in range(len(solutions)):
             solution_ids.append(solutions[i]["solutionId"])
            
    expected_response={
        
        "memberIds":solution_ids,
        "installed_solutions":installed_solutions,
        "offset":offset,
        
    }
    # if 'include_member' in qry_prm: 
    #     include_member=bool('include_member')
    #     print(include_member)
    if include_member==True:
       print("****")
       members=get_members(offset,limit)
       expected_response['members']=members
    print(json.dumps(expected_response,indent=4))

get_solutions(manifests,qry_prm)









