import pathlib
import sys
import json


#qry_prm="solutions?offset=0&limit=0"

#Convering query parameters into dict type
def query_params(qry_prm):
    d=dict()
    if "?" not in qry_prm:
       sys.exit("provided Bad parameters")
    elif len(qry_prm)!=0 and "?" in qry_prm:
      qry_prm=qry_prm.split("?")
      qry_prm="".join(qry_prm[1])
      qry_prm=qry_prm.split("&")
      for i in qry_prm:
        k,v=(i.split("="))
        d[k]=v
      return d





#Adding all manifest files into a tuple
def all_manifests():
    flist = []
    slist=[]
    member_ids=[]
    for p in pathlib.Path('.').iterdir():
        if p.is_file():
          #print(p)
          flist.append(p)
    for f in flist:
      with open(f) as fil:
        #print(fil.name)
        if(fil.name.endswith("json")):
            data=json.load(fil)
            slist.append(data)
    manifests=tuple(slist)
    return manifests
