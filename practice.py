import pathlib
import json
import sys
import req_params


manifests = req_params.get_manifests()


def get_members(offset, limit, qry_prm):
    '''Getting members data for individual member and returning in list'''

    keys_in_members = ["solutionId", "contextProfile",
                       "description", "registrations"]
    members_data = []
    for manifest in manifests[offset:offset+limit]:
        empty_member_data = {}
        for key in keys_in_members:
            empty_member_data[key] = manifest[key]
        members_data.append(empty_member_data)
    members = members_data
    return members


def get_solutionIds(offset, limit, solutions, installed_solutions):
    '''Getting solution_ids based on limit and offset'''
    
    solution_ids = []
    if offset < 0 or limit <= 0:
        print("provide query parameters are not valid")
        sys.exit()

    elif offset > installed_solutions:
        solution_ids = []
    else:
        limit = limit if limit != 0 else installed_solutions
        for i in range(offset, offset+limit):
            if i in range(len(list(solutions))):
                solution_ids.append(solutions[i]["solutionId"])
    return solution_ids


def get_solutions(solutions):
    '''This function will take user input and provide response based on request'''

    offset = 0
    solution_ids = []
    installed_solutions = len(list(solutions))
    limit = installed_solutions
    include_member = False
    
    #Reading query parameters from user
    qr_prms = input("Enter  query parameters for solutions endpoint :")
    
    #Convering query parameters to dict type
    qry_prm, content_Filters = req_params.get_query_params(qr_prms)
    

    #Assigning values to query parameters based on input else default values 
    if qry_prm != None:
        offset = int(qry_prm.get('offset', 0))
        limit = int(qry_prm.get('limit', installed_solutions))
        include_member = bool(qry_prm.get("include_member", False))
        select_solutionId = str(qry_prm.get('select_solutionId'))
    
    #Getting solution_ids based on offset and limit values
    solution_ids = get_solutionIds(
        offset, limit, solutions, installed_solutions)

    #Getting solution_id  details based on provided query parameter
    selected_solutionId_count = 0
    if 'select_solutionId' in qry_prm:
        for solution in solutions:
            if select_solutionId == solution['solutionId']:
                solution_ids = solution['solutionId']

    expected_response = {

        "links":
            {
                "hrefTemplate": "{self.solutionmanager.urls{solutionId}}",
                "rel": "solution"
            },
        "memberIds": solution_ids,
        "installed_solutions": installed_solutions,
        "offset": offset

    }
    
    #providing member data to expected response
    if include_member == True:
        members = get_members(offset, limit, qry_prm)
        if 'select_solutionId' in qry_prm:
            for member in members:
                if select_solutionId == member['solutionId']:
                    selected_solutionId_count += 1
                    expected_response['members'] = members[selected_solutionId_count]
        else:
            expected_response['members'] = members
    

    #providing only content filter data to expected response
    if 'contentFilter' in qry_prm:
        expected_response_Filters = {}

        for filter in content_Filters:
            expected_response_Filters[filter] = expected_response[filter]
        expected_response = expected_response_Filters


    #Generating json file to show output
    result = json.dumps(expected_response, indent=4)
    out_file = open("output/test.json", "w")
    out_file.write(result)
    out_file.close()


manifests = tuple(manifests)



get_solutions(manifests)
