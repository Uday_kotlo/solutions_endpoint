import pathlib
import sys
import json


#qry_prm="solutions?offset=0&limit=0"
def get_manifests():
    '''Adding manifest files into generator'''
    
    for file in pathlib.Path('.').iterdir():
        if file.is_file and file.name.endswith("json"):
          with open(file) as fil:
            data=json.load(fil)
            yield data
    

def get_query_params(qry_prm):
    '''Converting query parameters to dict type,
       Converting query parameters to list type
    '''

    qr_prms=dict()
    contentFilter=""
    if len(qry_prm)!=0 and "?" in qry_prm:
            qry_prm=qry_prm.split("?")
            qry_prm="".join(qry_prm[1])
            qry_prm=qry_prm.split("&")
            for item in qry_prm:
                      key,value=(item.split("="))
                      qr_prms[key]=value
            
    if "contentFilter" in qr_prms:
            contentFilter=qr_prms['contentFilter']
            contentFilter=contentFilter.replace("[","")
            contentFilter=contentFilter.replace("]","")
            contentFilter=contentFilter.replace("'","")
            contentFilter=contentFilter.split(",")
       
    return qr_prms,contentFilter
    
    
